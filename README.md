# Code Analyzer

Performs some basic code analysis on [Spiderbasic](https://www.spiderbasic.com/) and [PureBasic](https://www.purebasic.com/) files.

Came out of a conversation with a fellow dev about how much white space was typical when coding in certain languages, and evolved from there...

Written in [PureBasic](https://www.purebasic.com/).

